$(document).ready(function() {
    /* sidebar-brand */
    var trigger = $('.hamburger'),
    overlay = $('.overlay'),
    isClosed = false;

    trigger.click(function () {
        hamburger_cross();      
    });

    function hamburger_cross() {
        if (isClosed == true) {          
            overlay.hide();
            trigger.removeClass('is-open');
            trigger.addClass('is-closed');
            isClosed = false;
        } else {   
            overlay.show();
            trigger.removeClass('is-closed');
            trigger.addClass('is-open');
            isClosed = true;
        }
    }    

    $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
    });  

    $('.overlay').click(function () {
        overlay.hide();
        trigger.removeClass('is-open');
        trigger.addClass('is-closed');
        isClosed = false;
        $('#wrapper').toggleClass('toggled');
    });

    /*-------------------*/
    $("#owl-demo").owlCarousel({
        navigation : false, // Show next and prev buttons
        autoPlay : true,
        slideSpeed : 300,
        paginationSpeed : 900,
        singleItem:true,
        navigationText: [
        "<i class='icon-chevron-left icon-white'></i>",
        "<i class='icon-chevron-right icon-white'></i>"
        ],
    });

    $("#unit-list").owlCarousel({
        navigation : false,
        pagination : false,
        autoPlay: false,
        items : 4,
        slideSpeed : 300,
        paginationSpeed : 900,
        navigationText: [
        "<i class='icon-chevron-left icon-white'></i>",
        "<i class='icon-chevron-right icon-white'></i>"
        ]
    });

    $("#slider-text").owlCarousel({
        navigation : false, // Show next and prev buttons
        // autoPlay : true,
        slideSpeed : 300,
        paginationSpeed : 900,
        singleItem:true,
        navigationText: [
        "<i class='icon-chevron-left icon-white'></i>",
        "<i class='icon-chevron-right icon-white'></i>"
        ],
    });


    var width_windown = $(window).width();
    var width_content = $('.container').width();
    var btn_control_slider = (width_windown - width_content)/2;
    $(".owl-theme .owl-controls").css("padding-left",btn_control_slider);
    $(window).resize(function(){
        var width_windown = $(window).width();
        var width_content = $('.container').width();
        var btn_control_slider = (width_windown - width_content)/2;
        $(".owl-theme .owl-controls").css("padding-left",btn_control_slider);
    });

    // socialCircle
    $( ".socialCircle-center" ).socialCircle({
        // rotate: 250,
        // radius:120,
        // circleSize: 3,
        // speed:500
        rotate: 166,
        radius:120,
        circleSize: 3,
        speed:500
    });
});


jQuery(window).load(function(){
    var isiDevice = /ipad|iphone|ipod/i.test(navigator.userAgent.toLowerCase());
    var isAndroid = /android/i.test(navigator.userAgent.toLowerCase());
    var isBlackBerry = /blackberry/i.test(navigator.userAgent.toLowerCase());
    var isWindowsPhone = /windows phone/i.test(navigator.userAgent.toLowerCase());
    var isWebOS = /webos/i.test(navigator.userAgent.toLowerCase());

    endScroll = 7000;
    
    if(!isAndroid && isiDevice==false && isBlackBerry==false && isWindowsPhone==false && isWebOS==false){
        var s = skrollr.init({
            constants:{
                end: endScroll
            },
            render: function(data){
                //console.log(data.curTop);
            }
        });
    }
})