$(document).ready(function() {
	$("#owl-demo").owlCarousel({
		navigation : false, // Show next and prev buttons
		// autoPlay : true,
		slideSpeed : 300,
		paginationSpeed : 900,
		singleItem:true,
		navigationText: [
		"<i class='icon-chevron-left icon-white'></i>",
		"<i class='icon-chevron-right icon-white'></i>"
		],
	});

	$("#unit-list").owlCarousel({
		navigation : false,
		pagination : false,
		autoPlay: false,
		items : 4,
		slideSpeed : 300,
		paginationSpeed : 900,
		navigationText: [
		"<i class='icon-chevron-left icon-white'></i>",
		"<i class='icon-chevron-right icon-white'></i>"
		]
	});

	$("#slider-text").owlCarousel({
		navigation : false, // Show next and prev buttons
		// autoPlay : true,
		slideSpeed : 300,
		paginationSpeed : 900,
		singleItem:true,
		navigationText: [
		"<i class='icon-chevron-left icon-white'></i>",
		"<i class='icon-chevron-right icon-white'></i>"
		],
	});


	var width_windown = $(window).width();
	var width_content = $('.container').width();
	var btn_control_slider = (width_windown - width_content)/2;
	$(".owl-theme .owl-controls").css("padding-left",btn_control_slider);
	$(window).resize(function(){
		var width_windown = $(window).width();
		var width_content = $('.container').width();
		var btn_control_slider = (width_windown - width_content)/2;
		$(".owl-theme .owl-controls").css("padding-left",btn_control_slider);
	});

	// socialCircle
	$( ".socialCircle-center" ).socialCircle({
		// rotate: 250,
		// radius:120,
		// circleSize: 3,
		// speed:500

		rotate: 166,
		radius:120,
		circleSize: 3,
		speed:500
	});

});